#!/bin/bash

SCRIPTS_DIR=$(pwd)
SRC_DIR="${SCRIPTS_DIR}/../src"
# TESTS_DIR="${SRC_DIR}/manual_tests"
COLORDATA_DIR="${SRC_DIR}/color_data"
COLOR_LOADING_DIR="${SRC_DIR}/color_loaders"

export PYTHONPATH="${SRC_DIR}"
# echo "${PYTHONPATH}"

# cd "${COLOR_LOADING_DIR}"
cd "${SRC_DIR}"
python load_colors.py
