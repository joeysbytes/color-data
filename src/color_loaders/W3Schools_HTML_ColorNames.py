from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "W3Schools HTML Color Names",
          "description": "The list of official HTML color names.  This list has been verified to be the same as on other sites, but this one is nice because the names are in CamelCase.",
          "url": "https://www.w3schools.com/colors/colors_names.asp"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    color_names_list = load_color_names(web_page, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source) -> List[ColorName]:
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    for i in range(1, 149):
        div_tag = soup.find(name="div", attrs={"id": f"box{i}"})
        color_names_list.append(build_color_name(div_tag, source))
    return color_names_list


def build_color_name(div_tag, source: Source) -> ColorName:
    color_name = ColorName()
    color_name.source_id = source.source_id
    color_name.url = source.url
    color_name.palette = PALETTE

    # color name
    a_tag = div_tag.find(name="span", attrs={"class": "colornamespan"}).a
    color_name.name = a_tag.text.strip()

    # color hex
    a_tag = div_tag.find(name="span", attrs={"class": "colorhexspan"}).a
    color_name.hex_value = a_tag.text.strip().lower().replace("#", "")
    idx = int(color_name.hex_value, 16)
    color_name.palette_index = idx

    return color_name
