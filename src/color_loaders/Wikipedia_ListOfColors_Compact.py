import re
from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "Wikipedia List Of Colors (Compact)",
          "description": "All named colors on 1 single Wikipedia page",
          "url": "https://en.wikipedia.org/wiki/List_of_colors_(compact)"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    color_names_list = load_color_names(web_page, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source) -> List[ColorName]:
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    color_p_tags = soup.find_all(is_color_p)
    for color_p_tag in color_p_tags:
        color_names_list.append(build_color_name(color_p_tag, source))
    return color_names_list


def is_color_p(tag) -> bool:
    title_pattern = r'#[0-9A-Fa-f]{6}$'
    if ((tag.name == "p") and
            (tag.has_attr("title")) and
            (tag.has_attr("style")) and
            (re.search(title_pattern, tag["title"]))):
        return True
    return False


def build_color_name(p_tag, source: Source) -> ColorName:
    title_pattern = r'#[0-9A-Fa-f]{6}$'
    color_name = ColorName()
    color_name.source_id = source.source_id
    color_name.url = source.url
    color_name.palette = PALETTE
    hex_match = re.search(title_pattern, p_tag["title"])
    hex_code = hex_match.group(0)[1:]
    color_name.hex_value = hex_code.lower()
    idx = int(color_name.hex_value, 16)
    color_name.palette_index = idx

    # Move to the next following a tag, which has the name
    next_p_tag = p_tag.next_sibling.next_sibling
    a_tag = next_p_tag.a
    if a_tag is None:
        color_name.name = next_p_tag.text.strip()
    else:
        color_name.name = a_tag.text.strip()
    return color_name
