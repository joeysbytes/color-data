from color_database.Sources import Source
from color_database import ColorNames
from common import requests_helper
from color_loaders.Encycolorpedia_WebSafeColors import load_color_names


SOURCE = {"name": "Encycolorpedia Named Colors",
          "description": "Encycolorpedia Named Color Codes",
          "url": "https://encycolorpedia.com/named"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    msg = f"Parsing: {SOURCE['name']}"
    color_names_list = load_color_names(web_page, source, msg)
    ColorNames.insert_list_of_color_names(color_names_list, source)
