from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "Wikipedia X11 Color Names",
          "description": "Wikipedia list of X11 color names, with some alternatives",
          "url": "https://en.wikipedia.org/wiki/X11_color_names"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    color_names_list = load_color_names(web_page, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source) -> List[ColorName]:
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    table_tag = soup.find(is_x11_table)
    table_body_tag = table_tag.tbody
    table_row_tags = table_body_tag.find_all("tr")
    # Have to skip over the first tr row, since it is the headings
    for tr_tag in table_row_tags[1:]:
        color_names = get_color_names(tr_tag)
        for color_name in color_names:
            color_names_list.append(build_color_name(tr_tag, color_name, source))
    return color_names_list


def is_x11_table(tag) -> bool:
    if tag.name == "table":
        caption_tag = tag.caption
        if caption_tag.string.startswith("X11 color names"):
            return True
    return False


def get_color_names(tr_tag) -> List[str]:
    name_list = []
    column_tags = list(tr_tag.children)

    # Get main name
    name_col = column_tags[1]
    span_tag = name_col.span
    a_tag = span_tag.a
    if a_tag is None:
        name_list.append(span_tag.text.strip())
    else:
        name_list.append(a_tag.text.strip())

    # Alternative name(s)
    # Note, the alternate names are in the main table, so looping through them causes duplicate entries.
    # This logic is not needed.
    # if len(column_tags) > 20:
    #     alt_name_col = column_tags[21]
    #     alt_name_text = alt_name_col.text
    #     alt_names = alt_name_text.split(",")
    #     for alt_name in alt_names:
    #         name_list.append(alt_name.strip())

    return name_list


def build_color_name(tr_tag, color_nm: str, source: Source) -> ColorName:
    color_name = ColorName()
    color_name.source_id = source.source_id
    color_name.url = source.url
    color_name.palette = PALETTE
    color_name.name = color_nm

    # hex value
    column_tags = list(tr_tag.children)
    hex_col = column_tags[3]
    color_name.hex_value = hex_col.text.strip()[1:].lower()

    idx = int(color_name.hex_value, 16)
    color_name.palette_index = idx

    return color_name
