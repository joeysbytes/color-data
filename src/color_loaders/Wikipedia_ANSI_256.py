import re
from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "Wikipedia ANSI 256-Color RGB Values",
          "description": "256-color palette as listed by Wikipedia, which does seem to match my terminal output.  The first 16 colors are still terminal settings dependent.",
          "url": "https://en.wikipedia.org/wiki/ANSI_escape_code"
          }
PALETTE = "256"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    color_names_list = load_color_names(web_page, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source) -> List[ColorName]:
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    # Get 256 color table data cells
    td_list = soup.find_all(is_256_color_td)
    if len(td_list) != 256:
        msg = f"Did not find correct number of entries: {len(td_list)}"
        raise RuntimeError(msg)
    # Build color names list
    for td_tag in td_list:
        color_names_list.append(build_color_name(td_tag, source))
    return color_names_list


def is_256_color_td(tag) -> bool:
    title_pattern = r'^#[0-9a-f]{6}$'
    if ((tag.name == "td") and
            (tag.has_attr("style")) and
            (tag.has_attr("title")) and
            (re.match(title_pattern, tag["title"]))):
        try:
            value = int(tag.text)
            if 0 <= value <= 255:
                return True
        except Exception as e:
            pass
    return False


def build_color_name(td_tag, source: Source) -> ColorName:
    color_name = ColorName()
    color_name.hex_value = td_tag["title"][1:].lower()
    color_name.source_id = source.source_id
    idx = int(td_tag.text)
    color_name.name = f"Index {idx}"
    color_name.url = source.url
    color_name.palette = PALETTE
    color_name.palette_index = idx
    return color_name
