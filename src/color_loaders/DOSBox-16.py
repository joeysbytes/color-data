from typing import Dict, List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "DOSBox 16-Color Values",
          "description": "Colors retrieved from a DOSBox window with a color picker, and verified on several internet websites.",
          "url": "https://gitlab.com/joeysbytes/color-data/-/raw/main/raw_data/dosbox_16_colors.json"
          }
PALETTE = "16"


def run(source: Source) -> None:
    """Run the DOSBox-16 data loading"""
    color_dict = requests_helper.download_url(source.url, "json")
    color_names_list = load_color_names(color_dict, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(color_dict: Dict, source: Source) -> List[ColorName]:
    """Given the color dictionary data, create a list of ColorName objects."""
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    for color_info in color_dict["colors"]:
        color_name = ColorName()
        color_name.hex_value = color_info["hex"].lower()
        color_name.source_id = source.source_id
        color_name.name = color_info["color"].strip()
        color_name.url = source.url
        color_name.palette = PALETTE
        color_name.palette_index = color_info["ansi_index"]
        color_names_list.append(color_name)
    return color_names_list
