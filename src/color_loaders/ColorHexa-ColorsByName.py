from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "ColorHexa Colors By Name",
          "description": "ColorHexa Colors By Name",
          "url": "https://www.colorhexa.com/color-names"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    color_names_list = load_color_names(web_page, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source) -> List[ColorName]:
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    table_tag = soup.find(name="table", attrs={"class": "color-list"})
    table_rows = table_tag.tbody.find_all("tr")
    for table_row in table_rows:
        a_tag = table_row.td.a
        color_names_list.append(build_color_name(a_tag, source))
    return color_names_list


def build_color_name(a_tag, source: Source) -> ColorName:
    color_name = ColorName()
    color_name.hex_value = a_tag["href"][1:].lower()
    color_name.source_id = source.source_id
    color_name.name = a_tag.text.strip()
    color_name.url = source.url
    color_name.palette = PALETTE
    idx = int(color_name.hex_value, 16)
    color_name.palette_index = idx
    return color_name
