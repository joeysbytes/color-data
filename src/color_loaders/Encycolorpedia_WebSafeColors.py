from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "Encycolorpedia Web Safe Colors",
          "description": "Encycolorpedia Web Safe HTML Color Codes",
          "url": "https://encycolorpedia.com/websafe"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    msg = f"Parsing: {SOURCE['name']}"
    color_names_list = load_color_names(web_page, source, msg)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source, msg: str) -> List[ColorName]:
    print(msg)
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    ol_tag = soup.find("ol")
    a_tag_list = ol_tag.find_all("a")
    for a_tag in a_tag_list:
        color_names_list.append(build_color_name(a_tag, source))
    return color_names_list


def build_color_name(a_tag, source: Source) -> ColorName:
    color_name = ColorName()
    text_parts = a_tag.text.split("#")
    color_name.hex_value = text_parts[1].lower()
    color_name.source_id = source.source_id
    idx = int(color_name.hex_value, 16)
    color_name.name = text_parts[0].strip()
    color_name.url = source.url
    color_name.palette = PALETTE
    color_name.palette_index = idx
    return color_name
