from typing import Dict, List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper


SOURCE = {"name": "DOSBox EGA-Color Values",
          "description": "EGA Colors retrieved from a DOSBox window with a color picker, and verified on several internet websites.",
          "url": "https://gitlab.com/joeysbytes/color-data/-/raw/main/raw_data/dosbox_ega_colors.json"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    """Run the DOSBox-EGA data loading"""
    color_dict = requests_helper.download_url(source.url, "json")
    color_names_list = load_color_names(color_dict, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(color_dict: Dict, source: Source) -> List[ColorName]:
    """Given the color dictionary data, create a list of ColorName objects."""
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    for color_info in color_dict["colors"]:
        color_name = ColorName()
        color_name.hex_value = color_info["hex"].lower()
        color_name.source_id = source.source_id
        color_name.name = "Color " + str(color_info["index"])
        color_name.url = source.url
        color_name.palette = PALETTE
        idx = int(color_name.hex_value, 16)
        color_name.palette_index = idx
        color_names_list.append(color_name)
    return color_names_list
