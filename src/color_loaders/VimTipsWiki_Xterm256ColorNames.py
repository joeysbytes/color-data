from bs4 import BeautifulSoup
from typing import List
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper

SOURCE = {"name": "Vim Tips Wiki Xterm 256 Color Names",
          "description": "From the Vim Tips wiki, a user created this list of console colors.",
          "url": "https://vim.fandom.com/wiki/Xterm256_color_names_for_console_Vim"
          }
PALETTE = "256"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    color_names_list = load_color_names(web_page, source)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source) -> List[ColorName]:
    print(f"Parsing: {SOURCE['name']}")
    color_names_list = []
    soup = BeautifulSoup(web_page, "html5lib")
    color_list = soup.pre
    for color_line in color_list.text.split('\n'):
        color_data = color_line.strip()
        if len(color_data) > 0:
            color_names_list.append(build_color_name(color_data, source))
    return color_names_list


def build_color_name(color_data, source: Source) -> ColorName:
    color_parts = color_data.split(" ")
    color_name = ColorName()
    color_name.source_id = source.source_id
    color_name.url = source.url
    color_name.palette = PALETTE

    # color name
    temp_parts = color_parts[1].split("_")
    color_name.name = temp_parts[1]

    # color index
    temp_parts = color_parts[2].split("=")
    color_name.palette_index = int(temp_parts[1])

    # color hex value
    temp_parts = color_parts[3].split("#")
    color_name.hex_value = temp_parts[1].lower()

    return color_name
