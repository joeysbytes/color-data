from typing import List, Tuple
from color_database.Sources import Source
from color_database.ColorNames import ColorName
from color_database import ColorNames
from common import requests_helper

SOURCE = {"name": "XOrg rgb.txt",
          "description": "Official XOrg (X11) rgb.txt color names",
          "url": "https://gitlab.freedesktop.org/xorg/app/rgb/-/raw/master/rgb.txt"
          }
PALETTE = "RGB"


def run(source: Source) -> None:
    web_page = requests_helper.download_url(source.url)
    msg = f"Parsing: {SOURCE['name']}"
    color_names_list = load_color_names(web_page, source, msg)
    ColorNames.insert_list_of_color_names(color_names_list, source)


def load_color_names(web_page: str, source: Source, msg: str) -> List[ColorName]:
    print(msg)
    color_names_list = []
    color_data_list = split_color_data(web_page)
    for color_data in color_data_list:
        color_names_list.append(build_color_name(color_data, source))
    return color_names_list


def split_color_data(web_data: str) -> List:
    color_data_list = []
    for data_line in web_data.split('\n'):
        if len(data_line) > 0:
            data_line = data_line.replace('\t', ' ')
            red = int(data_line[0:3].strip())
            green = int(data_line[4:7].strip())
            blue = int(data_line[8:11].strip())
            name = data_line[11:].strip()
            color_data_list.append((red, green, blue, name))
    return color_data_list


def build_color_name(color_data: Tuple, source: Source):
    color_name = ColorName()
    hex_code = ""
    for i in range(0, 3):
        hex_code += f"{color_data[i]:02x}"
    color_name.hex_value = hex_code.lower()
    color_name.source_id = source.source_id
    color_name.name = color_data[3].strip()
    color_name.url = source.url
    color_name.palette = PALETTE
    idx = int(color_name.hex_value, 16)
    color_name.palette_index = idx
    return color_name
