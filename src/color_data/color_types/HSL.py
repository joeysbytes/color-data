class HSL:

    def __init__(self, hue, saturation, light):
        if 0.0 <= hue < 360.0:
            self._hue = hue
        else:
            raise ValueError(f"Invalid hue value: {hue}")
        if 0.0 <= saturation <= 1.0:
            self._saturation = saturation
        else:
            raise ValueError(f"Invalid saturation value: {saturation}")
        if 0.0 <= light <= 1.0:
            self._light = light
        else:
            raise ValueError(f"Invalid light value: {light}")

    def __str__(self):
        desc =    f"Hue:        {round(self.hue, 2)} degrees"
        desc += f"\nSaturation: {round(self.saturation * 100, 2)} %"
        desc += f"\nLight:      {round(self.light * 100, 2)} %"
        desc += f"\nHSL:        {self.hsl}"
        return desc

    @property
    def hue(self):
        return self._hue

    @property
    def saturation(self):
        return self._saturation

    @property
    def light(self):
        return self._light

    @property
    def hsl(self):
        return self.hue, self.saturation, self.light

    def to_rgb(self):
        # https://www.rapidtables.com/convert/color/hsl-to-rgb.html
        from color_data.color_types.RGB import RGB
        sixty_degs = 60
        c = (1 - abs((2 * self.light) - 1)) * self.saturation
        x = c * (1 - abs(((self.hue / sixty_degs) % 2) - 1))
        m = self.light - (c / 2)
        r1, g1, b1 = None, None, None
        if 0.0 <= self.hue < 60.0:
            r1, g1, b1 = c, x, 0.0
        elif 60.0 <= self.hue < 120.0:
            r1, g1, b1 = x, c, 0.0
        elif 120.0 <= self.hue < 180.0:
            r1, g1, b1 = 0.0, c, x
        elif 180.0 <= self.hue < 240.0:
            r1, g1, b1 = 0.0, x, c
        elif 240.0 <= self.hue < 300.0:
            r1, g1, b1 = x, 0.0, c
        elif 300.0 <= self.hue < 360.0:
            r1, g1, b1 = c, 0.0, x
        red = round((r1 + m) * 255)
        green = round((g1 + m) * 255)
        blue = round((b1 + m) * 255)
        return RGB(red, green, blue)
