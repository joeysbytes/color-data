from color_data.color_types.CMYK import CMYK
from color_data.color_types.HSL import HSL
from color_data.color_types.HSV import HSV


class RGB:

    def __init__(self, red, green, blue):
        if 0 <= red <= 255:
            self._red = red
        else:
            raise ValueError(f"Invalid red value: {red}")
        if 0 <= green <= 255:
            self._green = green
        else:
            raise ValueError(f"Invalid green value: {green}")
        if 0 <= blue <= 255:
            self._blue = blue
        else:
            raise ValueError(f"Invalid blue value: {blue}")

    def __str__(self):
        desc =    f"Red:       {self.red}"
        desc += f"\nGreen:     {self.green}"
        desc += f"\nBlue:      {self.blue}"
        desc += f"\nRGB:       {self.rgb}"
        desc += f"\nHex:       {self.hex_code}"
        desc += f"\nInt:       {self.int_code}"
        desc += f"\nRed Pct:   {self.red_pct * 100} %"
        desc += f"\nGreen Pct: {self.green_pct * 100} %"
        desc += f"\nBlue Pct:  {self.blue_pct * 100} %"
        desc += f"\nRGB Pct:   {self.rgb_pct}"
        return desc

    @property
    def red(self):
        return self._red

    @property
    def green(self):
        return self._green

    @property
    def blue(self):
        return self._blue

    @property
    def rgb(self):
        return self.red, self.green, self.blue

    @property
    def red_pct(self):
        return self.red / 255

    @property
    def green_pct(self):
        return self.green / 255

    @property
    def blue_pct(self):
        return self.blue / 255

    @property
    def rgb_pct(self):
        return self.red_pct, self.green_pct, self.blue_pct

    @property
    def hex_code(self):
        hex_cd = ""
        for color_value in self.rgb:
            hex_cd += f"{color_value:02x}"
        return hex_cd

    @property
    def int_code(self):
        return int(self.hex_code, 16)

    def to_cmyk(self):
        # https://www.rapidtables.com/convert/color/rgb-to-cmyk.html
        r1 = self.red / 255
        g1 = self.green / 255
        b1 = self.blue / 255
        c, m, y, k = None, None, None, None
        k = 1 - max(r1, g1, b1)
        try:
            c = (1 - r1 - k) / (1 - k)
        except ZeroDivisionError:
            c = 0.0
        try:
            m = (1 - g1 - k) / (1 - k)
        except ZeroDivisionError:
            m = 0.0
        try:
            y = (1 - b1 - k) / (1 - k)
        except ZeroDivisionError:
            y = 0.0
        return CMYK(c, m, y, k)

    def to_hsl(self):
        # https://www.rapidtables.com/convert/color/rgb-to-hsl.html
        r1 = self.red / 255
        g1 = self.green / 255
        b1 = self.blue / 255
        c_max = max(r1, g1, b1)
        c_min = min(r1, g1, b1)
        delta = c_max - c_min
        l = (c_max + c_min) / 2
        s = None
        if delta == 0.0:
            s = 0.0
        else:
            s = delta / (1 - abs((2 * l) - 1))
            if s > 1.0:
                s = 1.0
        h = None
        sixty_degs = 60
        if delta == 0.0:
            h = 0.0
        elif c_max == r1:
            h = sixty_degs * (((g1 - b1) / delta) % 6)
        elif c_max == g1:
            h = sixty_degs * (((b1 - r1) / delta) + 2)
        elif c_max == b1:
            h = sixty_degs * (((r1 - g1) / delta) + 4)
        return HSL(h, s, l)

    def to_hsv(self):
        # https://www.rapidtables.com/convert/color/rgb-to-hsv.html
        r1 = self.red / 255
        g1 = self.green / 255
        b1 = self.blue / 255
        c_max = max(r1, g1, b1)
        c_min = min(r1, g1, b1)
        delta = c_max - c_min
        v = c_max
        s = None
        if c_max == 0.0:
            s = 0.0
        else:
            s = delta / c_max
        h = None
        sixty_degs = 60
        if delta == 0.0:
            h = 0.0
        elif c_max == r1:
            h = sixty_degs * (((g1 - b1) / delta) % 6)
        elif c_max == g1:
            h = sixty_degs * (((b1 - r1) / delta) + 2)
        elif c_max == b1:
            h = sixty_degs * (((r1 - g1) / delta) + 4)
        return HSV(h, s, v)
