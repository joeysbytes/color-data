class HSV:

    def __init__(self, hue, saturation, value):
        if 0.0 <= hue < 360.0:
            self._hue = hue
        else:
            raise ValueError(f"Invalid hue value: {hue}")
        if 0.0 <= saturation <= 1.0:
            self._saturation = saturation
        else:
            raise ValueError(f"Invalid saturation value: {saturation}")
        if 0.0 <= value <= 1.0:
            self._value = value
        else:
            raise ValueError(f"Invalid value value: {value}")

    def __str__(self):
        desc =    f"Hue:        {round(self.hue, 2)} degrees"
        desc += f"\nSaturation: {round(self.saturation * 100, 2)} %"
        desc += f"\nValue:      {round(self.value * 100, 2)} %"
        desc += f"\nHSV:        {self.hsv}"
        return desc

    @property
    def hue(self):
        return self._hue

    @property
    def saturation(self):
        return self._saturation

    @property
    def value(self):
        return self._value

    @property
    def hsv(self):
        return self.hue, self.saturation, self.value

    def to_rgb(self):
        # https://www.rapidtables.com/convert/color/hsv-to-rgb.html
        from color_data.color_types.RGB import RGB
        sixty_degs = 60
        c = self.value * self.saturation
        x = c * (1 - abs(((self.hue / sixty_degs) % 2) - 1))
        m = self.value - c
        r1, g1, b1 = None, None, None
        if 0.0 <= self.hue < 60.0:
            r1, g1, b1 = c, x, 0.0
        elif 60.0 <= self.hue < 120.0:
            r1, g1, b1 = x, c, 0.0
        elif 120.0 <= self.hue < 180.0:
            r1, g1, b1 = 0.0, c, x
        elif 180.0 <= self.hue < 240.0:
            r1, g1, b1 = 0.0, x, c
        elif 240.0 <= self.hue < 300.0:
            r1, g1, b1 = x, 0.0, c
        elif 300.0 <= self.hue < 360.0:
            r1, g1, b1 = c, 0.0, x
        red = round((r1 + m) * 255)
        green = round((g1 + m) * 255)
        blue = round((b1 + m) * 255)
        return RGB(red, green, blue)
