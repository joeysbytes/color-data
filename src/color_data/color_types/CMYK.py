class CMYK:

    def __init__(self, cyan, magenta, yellow, black):
        if 0.0 <= cyan <= 1.0:
            self._cyan = cyan
        else:
            raise ValueError(f"Invalid cyan value: {cyan}")
        if 0.0 <= magenta <= 1.0:
            self._magenta = magenta
        else:
            raise ValueError(f"Invalid magenta value: {magenta}")
        if 0.0 <= yellow <= 1.0:
            self._yellow = yellow
        else:
            raise ValueError(f"Invalid yellow value: {yellow}")
        if 0.0 <= black <= 1.0:
            self._black = black
        else:
            raise ValueError(f"Invalid black value: {black}")

    def __str__(self):
        desc =    f"Cyan:        {round(self.cyan * 100, 2)} %"
        desc += f"\nMagenta:     {round(self.magenta * 100, 2)} %"
        desc += f"\nYellow:      {round(self.yellow * 100, 2)} %"
        desc += f"\nBlack (Key): {round(self.black * 100, 2)} %"
        desc += f"\nCMYK:        {self.cmyk}"
        return desc

    @property
    def cyan(self):
        return self._cyan

    @property
    def magenta(self):
        return self._magenta

    @property
    def yellow(self):
        return self._yellow

    @property
    def black(self):
        return self._black

    @property
    def cmyk(self):
        return self.cyan, self.magenta, self.yellow, self.black

    def to_rgb(self):
        # https://www.rapidtables.com/convert/color/cmyk-to-rgb.html
        from color_data.color_types.RGB import RGB
        r = round(255 * (1 - self.cyan) * (1 - self.black))
        g = round(255 * (1 - self.magenta) * (1 - self.black))
        b = round(255 * (1 - self.yellow) * (1 - self.black))
        return RGB(r, g, b)
