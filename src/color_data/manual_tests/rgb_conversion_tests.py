"""
This test takes all RGB values and converts them to another color type.
Then that color type is converted back to RGB.
The 2 RGB objects are compared for equality, to help verify the conversion formulas are correct.
To run all 16 million colors only takes a few minutes.
"""

from color_data.color_types.RGB import RGB
from color_data.color_types.CMYK import CMYK
from color_data.color_types.HSL import HSL

# Most of these are prime numbers
NUMBERS = (
    0, 1, 255,
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,
    101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199,
    211, 223, 227, 229, 233, 239, 241, 251)


def main():
    counter = 0
    # for red in NUMBERS:
    #     for green in NUMBERS:
    #         for blue in NUMBERS:
    for red in range(0, 256):
        for green in range(0, 256):
            for blue in range(0, 256):
                counter += 1
                run_test(red, green, blue, counter)


def run_test(red, green, blue, counter):
    if counter % 10000 == 0:
        print(f"Counter: {counter}, Testing: {red}, {green}, {blue}")

    # Create the original RGB color
    rgb = None
    try:
        rgb = RGB(red, green, blue)
    except Exception:
        msg = f"Failed to create original RGB color: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Build CMYK color
    cmyk = None
    try:
        cmyk = rgb.to_cmyk()
    except Exception:
        msg = f"Failed to create color type CMYK from RGB: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Build RGB color from CMYK color
    rgb_2 = None
    try:
        rgb_2 = cmyk.to_rgb()
    except Exception:
        msg = f"Failed to create color type RGB from CMYK: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Check CMYK
    check_conversion("CMYK", rgb, rgb_2)


    # Build HSL color
    hsl = None
    try:
        hsl = rgb.to_hsl()
    except Exception:
        msg = f"Failed to create color type HSL from RGB: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Build RGB color from CMYK color
    rgb_2 = None
    try:
        rgb_2 = hsl.to_rgb()
    except Exception:
        msg = f"Failed to create color type RGB from HSL: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Check HSL
    check_conversion("HSL", rgb, rgb_2)


    # Build HSV color
    hsv = None
    try:
        hsv = rgb.to_hsv()
    except Exception:
        msg = f"Failed to create color type HSV from RGB: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Build RGB color from CMYK color
    rgb_2 = None
    try:
        rgb_2 = hsv.to_rgb()
    except Exception:
        msg = f"Failed to create color type RGB from HSV: {red}, {green}, {blue}"
        raise RuntimeError(msg)

    # Check HSV
    check_conversion("HSV", rgb, rgb_2)



def check_conversion(color_type_desc, rgb, rgb_2):
    # Check if the conversion worked in both directions
    if rgb.hex_code != rgb_2.hex_code:
        print(f"Conversion error for color type: {color_type_desc}")
        print(f"Original rgb values:  {rgb.red}, {rgb.green}, {rgb.blue}")
        print(f"Converted rgb values: {rgb_2.red}, {rgb_2.green}, {rgb_2.blue}")
        raise ValueError("Did not convert back to RGB correctly")


main()
