from color_data.color_types.RGB import RGB
from color_data.color_types.CMYK import CMYK
from color_data.color_types.HSL import HSL
from color_data.color_types.HSV import HSV

color = RGB(0xaa, 0xbb, 0x05)
print(color)
