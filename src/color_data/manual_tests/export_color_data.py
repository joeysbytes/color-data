from color_data.color_types.RGB import RGB
from color_data.color_types.CMYK import CMYK
from color_data.color_types.HSL import HSL
from color_data.color_types.HSV import HSV

EXPORT_FILE = "/data/color_data/color_data.csv"


def main():
    with open(EXPORT_FILE, 'w') as fh:
        counter = 0
        for red in range(0, 256):
            for green in range(0, 256):
                for blue in range(0, 256):
                    counter += 1
                    data = build_color_data(red, green, blue, counter)
                    fh.write(f"{data}\n")


def build_color_data(red, green, blue, counter):
    if counter % 10000 == 0:
        print(f"Counter: {counter}, ({red}, {green}, {blue})")
    rgb = RGB(red, green, blue)
    cmyk = rgb.to_cmyk()
    hsl = rgb.to_hsl()
    hsv = rgb.to_hsv()
    data = f'"{rgb.hex_code}","{rgb.int_code}","{rgb.red}","{rgb.green}","{rgb.blue}",'
    data += f'"{cmyk.cyan}","{cmyk.magenta}","{cmyk.yellow}","{cmyk.black}",'
    data += f'"{hsl.hue}","{hsl.saturation}","{hsl.light}",'
    data += f'"{hsv.hue}","{hsv.saturation}","{hsv.value}"'
    return data

main()