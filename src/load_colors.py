import importlib
from color_database.Database import Database
from color_database.Sources import get_source
from color_database import ColorNames

# You drive the whole data loading process with this variable
# DATA_TO_LOAD = "ColorHexa-ColorsByName"
# DATA_TO_LOAD = "DOSBox-16"
DATA_TO_LOAD = "DOSBox-EGA"
# DATA_TO_LOAD = "Encycolorpedia_HTML_Colors"
# DATA_TO_LOAD = "Encycolorpedia_NamedColors"
# DATA_TO_LOAD = "Encycolorpedia_WebSafeColors"
# DATA_TO_LOAD = "VimTipsWiki_Xterm256ColorNames"
# DATA_TO_LOAD = "W3Schools_HTML_ColorNames"
# DATA_TO_LOAD = "Wikipedia_ANSI_256"
# DATA_TO_LOAD = "Wikipedia_ListOfColors_Compact"
# DATA_TO_LOAD = "Wikipedia_X11_ColorNames"
# DATA_TO_LOAD = "XOrg_RGB_txt"


def main():
    """The main processing logic"""
    print(f"Loading Color Data: {DATA_TO_LOAD}")
    database = Database.instance()
    try:
        database.open_db()
        # Load the module that loads the color data
        color_loader_module = importlib.import_module(f"color_loaders.{DATA_TO_LOAD}")
        # Get / Create a source record for the color data
        source_config = getattr(color_loader_module, "SOURCE")
        source = get_source(source_config)
        # Delete any previously loaded color name data
        ColorNames.remove_all_color_data_by_source_id(source.source_id)
        # Run the color loader
        runner = getattr(color_loader_module, "run")
        runner(source)
    except Exception as e:
        raise e
    finally:
        database.close_db()


if __name__ == "__main__":
    main()
