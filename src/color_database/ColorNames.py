from typing import List
from color_database.Database import Database
from color_database.Sources import Source


class ColorName:

    def __init__(self):
        self.hex_value = None
        self.source_id = None
        self.name = None
        self.url = None
        self.palette = None
        self.palette_index = None

    def __str__(self):
        desc = f"hex_value: {self.hex_value}, source_id: {self.source_id}, "
        desc += f"name: {self.name}, url: {self.url}, "
        desc += f"palette: {self.palette}, palette_index: {self.palette_index}"
        return desc

    def insert(self, cursor) -> None:
        sql = ("INSERT INTO color_names " +
               "(hex_value, source_id, name, url, palette, palette_index) " +
               "VALUES (?, ?, ?, ?, ?, ?)")
        values = (self.hex_value, self.source_id, self.name, self.url, self.palette, self.palette_index)
        cursor.execute(sql, values)

    @staticmethod
    def delete_by_source_id(source_id: int, cursor):
        sql = ("DELETE FROM color_names " +
               "WHERE source_id = ?")
        values = (source_id,)
        cursor.execute(sql, values)


###########################################################################
# Utilities
###########################################################################

def insert_list_of_color_names(color_names_list: List[ColorName], source: Source) -> None:
    """Given a list of ColorName objects, insert them in to the database."""
    print(f"Inserting list of color names: {len(color_names_list)} items")
    color_name = None
    database = Database.instance()
    try:
        cursor = database.open_cursor()
        for color_name in color_names_list:
            color_name.insert(cursor)
        source.update_color_names_loaded(cursor)
        database.commit_db()
    except Exception as e:
        print(f"  Failed to insert color: {color_name}")
        database.rollback_db()
        raise e
    finally:
        database.close_cursor()


def remove_all_color_data_by_source_id(source_id: int) -> None:
    """Given a source, remove all color data for it (for reloading scenarios)."""
    print(f"Removing previously loaded color names for source_id: {source_id}")
    database = Database.instance()
    try:
        cursor = database.open_cursor()
        ColorName.delete_by_source_id(source_id, cursor)
        database.commit_db()
    except Exception as e:
        print(f"  Failed to delete color names for source_id: {source_id}")
        database.rollback_db()
        raise e
    finally:
        database.close_cursor()
