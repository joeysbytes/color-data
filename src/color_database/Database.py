from typing import Any
import mariadb
from common.json_helper import load_json_file


class Database:
    # This JSON file contains the configuration needed to connect to the database
    DB_CONFIG_FILE = "/data/credentials/mariadb_colors.json"

    __instance = None

    @classmethod
    def instance(cls):
        if cls.__instance is None:
            cls.__instance = Database()
        return cls.__instance

    def __init__(self):
        if Database.__instance is not None:
            raise RuntimeError("Use instance() to get Database singleton.")
        db_config_data = load_json_file(Database.DB_CONFIG_FILE)
        self.is_open = False
        self.user = db_config_data["user"]
        self.__password = db_config_data["password"]
        self.database = db_config_data["database"]
        self.port = int(db_config_data["port"])
        self.host = db_config_data["host"]
        self._db_connection = None
        self.is_cursor_open = False
        self._cursor = None

    def open_db(self) -> None:
        """Connect to the database."""
        print("Connecting to database")
        if not self.is_open:
            try:
                self._db_connection = mariadb.connect(user=self.user,
                                                      password=self.__password,
                                                      database=self.database,
                                                      port=self.port,
                                                      host=self.host)
                self.is_open = True
                print("  Connected to database")
            except Exception as e:
                # Something happened trying to connect, let's clean up
                print("  Failed to connect to database")
                self.close_db(force=True)
                raise e
        else:
            print("  Database already connected")

    def close_db(self, force: bool = False) -> None:
        """Close the database connection."""
        print("Disconnecting from database")
        if self.is_open or force:
            try:
                if self._db_connection is not None:
                    self._db_connection.close()
                print("  Disconnected from database")
            except Exception as e:
                # Failing to close
                print("  Failed to disconnect from database")
                raise e
            finally:
                self._db_connection = None
                self.is_open = False
        else:
            print("  Database already disconnected")

    def commit_db(self) -> None:
        """Perform a database commit."""
        print("Performing database commit")
        try:
            self._db_connection.commit()
            print("  Database commit successful")
        except Exception as e:
            print("  Failed to commit database")
            raise e

    def rollback_db(self) -> None:
        """Perform a database rollback."""
        print("Performing database rollback")
        try:
            self._db_connection.rollback()
            print("  Database rollback successful")
        except Exception as e:
            print("  Failed to rollback database")
            raise e

    def open_cursor(self) -> Any:
        """Get a cursor from the database."""
        print("Getting database cursor")
        if not self.is_cursor_open:
            try:
                self._cursor = self._db_connection.cursor()
                self.is_cursor_open = True
                print("  Cursor opened")
            except Exception as e:
                print("  Failed to get database cursor")
                self.close_cursor(force=True)
                raise e
        else:
            print("  Cursor already opened")
        return self._cursor

    def close_cursor(self, force=False) -> None:
        """Close the cursor from the database."""
        print("Closing database cursor")
        if self.is_cursor_open or force:
            try:
                if self._cursor is not None:
                    self._cursor.close()
            except Exception as e:
                print("  Failed to close cursor")
                raise e
            finally:
                self.is_cursor_open = False
                self._cursor = None
        else:
            print("  Cursor already closed")
