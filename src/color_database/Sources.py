from typing import Dict, Type
from color_database.Database import Database
from datetime import datetime


class Source:

    def __init__(self):
        self.source_id = None
        self.name = None
        self.url = None
        self.description = None
        self.color_names_loaded = None

    def __str__(self):
        desc = f"source_id: {self.source_id}, name: {self.name}, "
        desc += f"url: {self.url}, description: {self.description}, "
        desc += f"color_names_loaded: {self.color_names_loaded}"
        return desc

    def select_by_name(self, cursor) -> None:
        sql = ("SELECT source_id, name, url, description, color_names_loaded " +
               "FROM sources " +
               "WHERE name = ?")
        values = (self.name,)
        cursor.execute(sql, values)
        row = cursor.fetchone()
        if row is not None:
            self.source_id = row[0]
            self.name = row[1]
            self.url = row[2]
            self.description = row[3]
            self.color_names_loaded = row[4]

    def insert(self, cursor):
        self.source_id = None
        sql = ("INSERT INTO sources " +
               "(name, url, description, color_names_loaded) " +
               "VALUES (?, ?, ?, ?)")
        values = (self.name, self.url, self.description, self.color_names_loaded)
        cursor.execute(sql, values)

    def update_by_id(self, cursor):
        sql = ("UPDATE sources " +
               "SET name = ?, " +
               "url = ?, " +
               "description = ?, " +
               "color_names_loaded = ? "
               "WHERE source_id = ?")
        values = (self.name, self.url, self.description, self.color_names_loaded, self.source_id)
        cursor.execute(sql, values)

    def update_color_names_loaded(self, cursor):
        """Update color_names_loaded with current timestamp."""
        sql = ("UPDATE sources " +
               "SET color_names_loaded = ? "
               "WHERE source_id = ?")

        values = (datetime.now(), self.source_id)
        cursor.execute(sql, values)


###########################################################################
# Utilities
###########################################################################

def get_source(source_config: Dict) -> Source:
    """Get the source record for the data about to be loaded."""
    print("Getting source record...")
    source = None

    # Get config data
    source_name = source_config["name"]
    source_url = None
    source_description = None
    if "url" in source_config:
        source_url = source_config["url"]
    if "description" in source_config:
        source_description = source_config["description"]

    database = Database.instance()
    cursor = None
    try:
        # Check if source record already exists
        print("  Checking if source already exists")
        source = Source()
        source.name = source_name
        cursor = database.open_cursor()
        source.select_by_name(cursor)

        # Source does not exist, create a new record
        if source.source_id is None:
            print("  Source not found, inserting new source")
            source = Source()
            source.name = source_name
            source.url = source_url
            source.description = source_description
            source.insert(cursor)
            database.commit_db()
            # re-read the record to get the auto-generated id
            source.select_by_name(cursor)

        else:
            # Check if database needs updated from the config file
            print("  Source found, checking if updates are needed")
            if ((source.url != source_url) or
               (source.description != source_description)):
                print("  Updating source record from config data")
                source.name = source_name
                source.url = source_url
                source.description = source_description
                source.update_by_id(cursor)
                database.commit_db()
            else:
                print("  No source updates needed")

        print(f"Source ID: {source.source_id}")

    except Exception as e:
        print("A fatal error occurred getting source")
        database.rollback_db()
        raise e
    finally:
        database.close_cursor()

    return source
