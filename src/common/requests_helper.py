import requests
from typing import Union, Dict


def download_url(url, data_type: str = None) -> Union[str, Dict]:
    """Given a url and expected data_type, download the data."""
    print(f"Downloading: {url}")
    response = requests.get(url)
    response.raise_for_status()
    data = None
    if data_type is None or data_type == "text":
        print("  Data type: text")
        data = response.text
    elif data_type == "json":
        print("  Data type: json")
        data = response.json()
    return data
