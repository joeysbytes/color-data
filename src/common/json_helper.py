import json


def load_json_file(json_file):
    """Given a JSON file name, load the file in to a Python dictionary and return it."""
    print(f"Loading JSON file: {json_file} ...")
    with open(json_file, 'r') as f:
        data_dict = json.load(f)
    print("  JSON file loaded")
    return data_dict
